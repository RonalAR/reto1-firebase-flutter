// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

Producto welcomeFromJson(String str) => Producto.fromJson(json.decode(str));

String welcomeToJson(Producto data) => json.encode(data.toJson());

class Producto {
    Producto({
        required this.name,
    });

    String name;

    factory Producto.fromJson(Map<String, dynamic> json) => Producto(
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
    };
}