import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AuthProvider {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final firestoreInstance = FirebaseFirestore.instance;
  void signInWithEmail(
      String email, String password, BuildContext context) async {
    if (email.isEmpty || password.isEmpty) {
      print("Email and password cannot be empty");
    } else {
      try {
        User? user = (await _auth.signInWithEmailAndPassword(
                email: email, password: password))
            .user;
        if (user != null) {
          print("ingresaste");
          Navigator.pushNamed(context, '/home');
        } else {
          print("USUARIO NO REGISTRADO");
        }
      } on FirebaseAuthException catch (e) {
        if (e.code == 'user-not-found') {
          print("CORREO NO REGISTRADO");
        } else if (e.code == 'wrong-password') {
          print("ERROR EN CONTRASEÑA");
        }
      }
    }
  }

  void verificarContrasena(String pass1, String pass2, String email,
      String nombre, BuildContext context) {
    if (nombre.isNotEmpty &&
        email.isNotEmpty &&
        pass1.isNotEmpty &&
        pass2.isNotEmpty) {
      if (validateNnombreUsuario(nombre, context)) {
        if (validateEmail(email, context)) {
          if (validatePassword(pass1, context) &&
              validatePassword(pass2, context)) {
            if (pass1 == pass2) {
              registrarUsuario(nombre, email, pass1, context);
            } else {
              print("CONTRASEÑAS NO IDENTICAS");
            }
          }
        }
      }
    } else {
      print("NO DEJE CAMPOS VACIOS");
    }
  }

  void verificarDatos(String pass1, String pass2, String email, String nombre,
      BuildContext context) {
    print("" + pass1 + "" + email + "" + nombre);
    if (nombre.isNotEmpty &&
        email.isNotEmpty &&
        pass1.isNotEmpty &&
        pass2.isNotEmpty) {
      if (validateNnombreUsuario(nombre, context)) {
        if (validateEmail(email, context)) {
          // if (validatePassword(pass1, context) &&
          //     validatePassword(pass2, context)) {
            if (pass1 == pass2) {
              registrarUsuario(nombre, email, pass1, context);
            } else {
              print("VERIFIQUE QUE LAS CONTRASEÑAS SEAN IDENTICAS");
            }
          // }
        }
      }
    } else {
      print("INGRESE TODOS LOS DATOS REQUERIDOS");
    }
  }

  void registrarUsuario(String nombreCompleto, String email, String password, BuildContext context) async {
    try {
      await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      ).whenComplete(() => 
        registrar (nombreCompleto,email,password, context)
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
    
  }

  void registrar(String nombreCompleto, String email, String password, BuildContext context) async {
    final FirebaseAuth auth = FirebaseAuth.instance;
    final User? user = auth.currentUser;
    final uid = user!.uid;

    firestoreInstance.collection("users").add({
      "name": nombreCompleto,
      "email": email,
      "pass": password,
      "uid" : uid
    }).then((value) {
      print(value.id);
      Navigator.pushNamed(context, '/');
    });
  }

  bool validateEmail(String value, BuildContext context) {
    bool res = false;
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = RegExp(pattern);
    if (regExp.hasMatch(value)) {
      res = true;
    } else {
      res = false;
      print("CORREO INVALIDO");
    }
    return res;
  }

  bool validatePassword(String value, BuildContext context) {
    bool res = false;
    String pattern = r'^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$';
    RegExp regExp = RegExp(pattern);
    if (regExp.hasMatch(value)) {
      res = true;
    } else {
      res = false;
      print("CONTRASEÑA INVALIDA");
    }
    return res;
  }

  bool validateNnombreUsuario(String value, BuildContext context) {
    bool res = false;
    String pattern = r'(^[a-zA-Z]*$)';
    RegExp regExp = RegExp(pattern);
    if (regExp.hasMatch(value)) {
      res = true;
    } else {
      res = false;
      print("NOMBRE INVALIDA");
    }
    return res;
  }

  redirigir(BuildContext context) {
    print("REGISTRO EXITOSO");
    Navigator.pushNamed(context, '/');
  }
}
