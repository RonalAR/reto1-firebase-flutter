import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:prueba_firestore/data/models/producto.dart';

class firebaseApi {
  CollectionReference collectionReference = FirebaseFirestore.instance.collection('products');
  final firestoreInstance = FirebaseFirestore.instance;
  Future<List<Producto>>  getProductos() async {
    QuerySnapshot producto = await collectionReference.get();
    List<Producto> _producto = [];
    if (producto.docs.length != 0) {
      for(var doc in producto.docs){
        _producto.add(Producto.fromJson(doc.data() as Map<String, dynamic>));
      }
    }
    return _producto;
  }

  Future<void> addProductos(Producto producto) {
    return collectionReference
      .add({
        'name': "producto prueba", 
      })
      .then((value) => print("product"))
      .catchError((error) => print("Failed to add product: $error"));
    }
}