import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:prueba_firestore/data/services/auth.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _emailLog = TextEditingController();
  final _pass = TextEditingController();
  @override

  FirebaseAuth datosUsuario = FirebaseAuth.instance;

  dynamic _userData;
  
  @override
  void initState() {
    super.initState();
    _checkIfLogged();
  }

  _checkIfLogged() async{
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        print('User is currently signed out!');
      } else {
        Navigator.pushNamed(context, '/home');
      }
    });
  }

 

  @override
  void dispose() {
    _emailLog.dispose();
    _pass.dispose();
    super.dispose();
  }

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Correo',
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 5.0),
        Container(
          alignment: Alignment.centerLeft,
          height: 60.0,
          child: TextField(
            controller: _emailLog,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.black,
              ),
              hintText: 'Ingresa tu correo electronico',
              hintStyle: TextStyle(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Contraseña',
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 5.0),
        Container(
          alignment: Alignment.centerLeft,
          height: 60.0,
          child: TextField(
            controller: _pass,
            obscureText: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.black,
              ),
              hintText: 'Ingresa tu contraseña',
              hintStyle: TextStyle(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildForgotPasswordBtn() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
        onPressed: () => Navigator.pushNamed(context, '/recuperarcuenta'),
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          'Olvidaste tu contraseña?',
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 50.0,
        vertical: 0.0,
      ),
      //padding: EdgeInsets.symmetric(vertical: 10.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () => loginEmailPass(),
        padding: EdgeInsets.all(11.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Color.fromRGBO(48, 94, 170, 132141),
        child: Text(
          'INGRESAR',
          style: TextStyle(
            color: Colors.white,
            letterSpacing: 1.5,
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  Widget _buildSignupBtn() {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, '/register'),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: 'No tienes una cuenta?    ',
              style: TextStyle(
                color: Colors.black,
                fontSize: 14.0,
                fontWeight: FontWeight.w400,
              ),
            ),
            TextSpan(
              text: '   Registrate',
              style: TextStyle(
                color: Colors.black,
                fontSize: 14.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Container(
              child: Column(
                  children: <Widget>[
                    Container(
                        height:  300,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(48, 94, 170, 132141),
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40), bottomRight: Radius.circular(40)),
                          boxShadow: [BoxShadow(
                            color: Colors.black,
                            blurRadius: 10.0,
                            offset: Offset(0,7)
                          )],
                        ),
                        child: Stack(
                            children: <Widget>[
                              Positioned(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 50),
                                    child: Center(
                                      child: Center(
                                          child: Text(
                                            "LOGIN",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 40,
                                                fontWeight:FontWeight.bold
                                            ),
                                          )
                                      ),
                                    ),
                                  )
                              )
                            ]
                        )
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20,top: 40, bottom: 0,right: 20),
                      child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(30),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color.fromRGBO(143, 148, 251, .2),
                                        blurRadius: 20.0,
                                        offset: Offset(0, 10)
                                    )
                                  ]
                              ),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    _buildEmailTF(),
                                    _buildPasswordTF(),
                                    _buildForgotPasswordBtn(),
                                    _buildLoginBtn(),
                                    // _buildSocialBtnRow(),
                                    SizedBox(height: 10.0),
                                    _buildSignupBtn(),
                                  ]
                              ),
                            )
                          ]
                      ),
                    ),
                  ]
              )
          ),
        )
    );
  }

  loginEmailPass() async {
    AuthProvider().signInWithEmail(_emailLog.text, _pass.text, context);
  }
}
