import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'componentes/body.dart';
import 'package:prueba_firestore/data/services/firestore_api.dart';
import 'package:prueba_firestore/provider/product_provider.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState  extends State<HomeScreen> {

  FirebaseAuth datosUsuario = FirebaseAuth.instance;
  productProvider productPro = productProvider();
  @override
  void initState (){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Productos'),
          actions: <Widget>[
            TextButton(
              onPressed: () => logout (), 
              child: Text('logout'),
              style: TextButton.styleFrom(primary: Colors.white),
            ),
            TextButton(
              onPressed: () => addProduct (), 
              child: Text('agregar producto'),
              style: TextButton.styleFrom(primary: Colors.white),
            )
          ],
        ),
        body: Body(),
    );
  }


  logout () async {
    await FirebaseAuth.instance.signOut().then((value) => Navigator.pushNamed(context, '/'));
  }

  addProduct () async {
    // productPro.setproduct("producto");
  }
}
