import 'package:prueba_firestore/data/models/producto.dart';
import 'package:prueba_firestore/ui/home/componentes/list_activities.dart';
import 'package:flutter/material.dart';
import 'package:prueba_firestore/data/services/firestore_api.dart';
import 'package:prueba_firestore/provider/product_provider.dart';


class HomeBody extends StatelessWidget {
  firebaseApi firestore = firebaseApi();
  productProvider productPro = productProvider();
  
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: productPro.getProducts,
      builder:(context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return Text('Press button to start');
          case ConnectionState.waiting:
            return Center(
              child: CircularProgressIndicator()
            );
          default: 
            if (snapshot.hasError)
              return Text('Error: ${snapshot.error}');
            else
              var datos = snapshot.data;
              List<Producto> products = snapshot.data as List<Producto>;
              return ListView.builder(
                itemCount: products.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      ListAtivities(
                        name: products[index].name,
                      )
                    ]
                  ); 
                }
            );   
        }
      },
    );
  }
}
