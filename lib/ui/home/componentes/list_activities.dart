import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class ListAtivities extends StatelessWidget {
  final String name;
  
  ListAtivities(
    {
      required this.name,
    }
  );

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 20, bottom: 10, right: 20, left: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(143, 148, 251, .2),
                  blurRadius: 20.0,
                  offset: Offset(0, 10)
              )
            ]
        ),
        child: 
            Card(
              color: Colors.grey[900],
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white70, width: 1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 70,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(
                                'assets/images/imageCard.jpg'),
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(20))
                      ),
                    ),
                    ListTile(
                      title: Text(
                        name,
                        style: TextStyle(fontSize: 18, color: Colors.white),
                        textAlign: TextAlign.center,
                      ), 
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}