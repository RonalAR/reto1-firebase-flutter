
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:prueba_firestore/data/services/auth.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  final _email = TextEditingController();
  final _nombreCompleto = TextEditingController();
  final _password = TextEditingController();
  final _passwordConf = TextEditingController();

  @override
  void dispose (){
    _email.dispose();
    _nombreCompleto.dispose();
    _password.dispose();
    _passwordConf.dispose();
    super.dispose();
  }

  Widget _buildFullNameRe(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Nombre de usuario',
          style:  TextStyle(color: Colors.black),
        ),
        SizedBox(height: 5.0),
        Container(
          alignment: Alignment.centerLeft,
          height: 60.0,
          child: TextField(
            controller: _nombreCompleto,
            keyboardType: TextInputType.name,

            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                    Icons.person_add,
                    color: Colors.black
                ),
                hintText: 'Ingresa su nombre',
                hintStyle: TextStyle(color: Colors.black)
            ),
          ),
        )
      ],
    );
  }

  Widget _buildEmailRe(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Correo',
          style:  TextStyle(color: Colors.black),
        ),
        SizedBox(height: 5.0),
        Container(
          alignment: Alignment.centerLeft,
          height: 60.0,
          child: TextField(
            controller: _email,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                  Icons.email,
                  color: Colors.black
              ),
              hintText: 'Ingresa tu correo electronico',
              hintStyle:  TextStyle(color: Colors.black),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildPasswordRe(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Contraseña',
          style:  TextStyle(color: Colors.black),
        ),
        SizedBox(height: 5.0),
        Container(
          alignment: Alignment.centerLeft,
          height: 60.0,
          child: TextField(
            controller: _password,
            obscureText: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                  Icons.lock_open,
                  color: Colors.black
              ),
              hintText: 'Ingresa tu contraseña',
              hintStyle:  TextStyle(color: Colors.black),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildConfirmPasswordRe(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Confirmar contraseña',
          style:  TextStyle(color: Colors.black),
        ),
        SizedBox(height: 5.0),
        Container(
          alignment: Alignment.centerLeft,
          height: 60.0,
          child: TextField(
            controller: _passwordConf,
            obscureText: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                  Icons.lock_open,
                  color: Colors.black
              ),
              hintText: 'Confirma tu contraseña',
              hintStyle:  TextStyle(color: Colors.black),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildRegistro() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 70.0,
        vertical: 0.0,
      ),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () => registrar(),
        padding: EdgeInsets.all(11.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Color.fromRGBO(48, 94, 170, 132141),
        child: Text(
          'REGISTRAR',
          style: TextStyle(
            color: Colors.white,
            letterSpacing: 1.5,
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Container(
              child: Column(
                  children: <Widget>[
                    Container(
                        height:  250,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(48, 94, 170, 132141),
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40), bottomRight: Radius.circular(40)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius: 10.0,
                              offset: Offset(0,7),
                            )
                          ]
                        ),
                        child: Stack(
                            children: <Widget>[
                              Positioned(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 50),
                                    child: Center(
                                      child: Center(
                                          child: Text(
                                            "REGISTRO",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 40,
                                                fontWeight:FontWeight.bold
                                            ),
                                          )
                                      ),
                                    ),
                                  )
                              )
                            ]
                        )
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20,top: 40, bottom: 0,right: 20),
                      child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(30),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color.fromRGBO(143, 148, 251, .2),
                                        blurRadius: 20.0,
                                        offset: Offset(0, 10)
                                    )
                                  ]
                              ),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    _buildFullNameRe(),
                                    _buildEmailRe(),
                                    _buildPasswordRe(),
                                    _buildConfirmPasswordRe(),
                                    SizedBox(
                                      height: 30.0,
                                    ),
                                    _buildRegistro(),
                                  ]
                              ),
                            )
                          ]
                      ),
                    ),
                  ]
              )
          ),
        )
    );
  }

  registrar() {
    AuthProvider().verificarDatos(
        _password.text,
        _passwordConf.text,
        _email.text ,
        _nombreCompleto.text,
        context
    );
  }
}
